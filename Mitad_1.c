/*
* Autor: Fatima Azucena MC
* Fecha: 20_01_2023
* Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>

int main(){

	//Declaracion de variables
	int numero;
	int doble;
	int tercera;
	int mitad;

	printf("Digite un número (0-200): ");
	scanf("%d", &numero);

	if ( ( numero > 0 ) && ( numero <=200 )){
		doble = numero * 2;
		tercera = doble / 3;
		mitad = tercera / 2;
		printf("La mitad de la tercera parte del doble de %d%s%d%s" , numero , "es: " , mitad , "\n");
	}
	else {
		printf("Número inválido");
	}

}
