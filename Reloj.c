//#include <SDL2/SDL.h>
#include <math.h>

#define SCREEN_WIDTH 600
#define SCREEN_HEIGHT 600
#define PI 3.1415926535

// Función para dibujar el reloj analógico
void drawClock(SDL_Renderer *renderer, int cx, int cy, int radius) {
    // Obtener la hora actual
    time_t rawtime;
    struct tm *timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    int hours = timeinfo->tm_hour % 12;
    int minutes = timeinfo->tm_min;
    int seconds = timeinfo->tm_sec;

    // Calcular ángulos para las manecillas
    float angleSeconds = -PI / 2 + (2 * PI * seconds) / 60;
    float angleMinutes = -PI / 2 + (2 * PI * minutes) / 60;
    float angleHours = -PI / 2 + (2 * PI * (hours * 60 + minutes)) / (12 * 60);

    // Dibujar el círculo del reloj
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderDrawCircle(renderer, cx, cy, radius);

    // Dibujar la manecilla de las horas
    int hourHandLength = radius * 0.5;
    int hourX = cx + hourHandLength * cos(angleHours);
    int hourY = cy + hourHandLength * sin(angleHours);
    SDL_RenderDrawLine(renderer, cx, cy, hourX, hourY);

    // Dibujar la manecilla de los minutos
    int minuteHandLength = radius * 0.7;
    int minuteX = cx + minuteHandLength * cos(angleMinutes);
    int minuteY = cy + minuteHandLength * sin(angleMinutes);
    SDL_RenderDrawLine(renderer, cx, cy, minuteX, minuteY);

    // Dibujar la manecilla de los segundos
    int secondHandLength = radius * 0.9;
    int secondX = cx + secondHandLength * cos(angleSeconds);
    int secondY = cy + secondHandLength * sin(angleSeconds);
    SDL_RenderDrawLine(renderer, cx, cy, secondX, secondY);

    // Actualizar la ventana
    SDL_RenderPresent(renderer);
}

int main() {
    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window *window = SDL_CreateWindow("Analog Clock", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    int quit = 0;
    SDL_Event event;

    while (!quit) {
        while (SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                quit = 1;
            }
        }

        // Dibujar el reloj analógico en cada iteración
        drawClock(renderer, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 200);
        SDL_Delay(1000); // Esperar 1 segundo antes de actualizar el reloj nuevamente
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}

