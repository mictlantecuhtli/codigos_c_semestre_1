/*
* Autor: Fatima Azucena MC
* Fecha: 20_01_2023
* Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>
int main (){

	//Declaración de variables
	int color;
	int carrera;
	int totalPagar = 0;
	int descuento = 0;
	int totalPagarDescuento = 0;

	printf("¿De qué color es la pelota que acabas de savar?:\n1.-Verde\n2.-Amarilla\n3.-Roja");
	printf("\nDigite su opción: ");
	scanf("%d", &color);

	if ( color == 1 ){
		printf("Felicidades, tienes un descuento del 10 porciento");
		printf("\n¿Cuál es tu monto a pagar?: ");
		scanf("%d", &totalPagar);
		descuento = totalPagar * 0.10;
		totalPagarDescuento = totalPagar - descuento;
	}
	else if ( color == 2 ){
		printf("Felicidades, tienes un descuento del 5 porciento");
                printf("\n¿Cuál es tu monto a pagar?: ");
                scanf("%d", &totalPagar);
                descuento = totalPagar * 0.05;
                totalPagarDescuento = totalPagar - descuento;
	}
	else if ( color == 3 ){
	 	printf("Felicidades, tienes un descuento del 15 porciento");
                printf("\n¿Cuál es tu monto a pagar?: ");
                scanf("%d", &totalPagar);
                descuento = totalPagar * 0.15;
                totalPagarDescuento = totalPagar - descuento;
	}
	else {
		printf("\nNúmero inválido");
	}

	printf("El total a pagar es: %d%s" , totalPagarDescuento , " pesos\n");

}
