/*
* Autor: Fatima Azucena MC
* Fecha: 20_01_2023
* Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>
int main (){

	//Declaración de variables
	int opcion;
	int cred = 0;
	int taller;
	int carrera;
	int lugar;

	printf("¿Participaste en el desfile?:\n1.-Si\n0.-No\nDigite su opción: ");
	scanf("%d", &opcion);

	if ( opcion == 1 ){
		cred = cred + 1;
		printf("Ganaste un crédito, ¿En qué tallar estas inscrito?: ");
		printf("\n1.-Danza\n2.-Basquetbol\n3.-Futbol\n4.-TKD\n5.-Voleibol\nDigite su opción: ");
		scanf("%d", &taller);

		if ( taller == 1 ){
			printf("Uniforme escolar");
		}
		else if ( taller == 2 ){
			printf("Blanco");
		}
		else if ( taller == 3 ){
			printf("Verde");
		}
		else if ( taller == 4){
			printf("Uniforme TKD");
		}
		else if ( taller == 5 ){
			printf("Rojo");
		}
		else {
			printf("Opción inválida");
		}
	
	}
	else {
		printf("No tienes ningun crédito");
	}

	printf("\n¿Participaste en la carrera?:\n1.-Si\n0.-No\nDigite su opción: ");
	scanf("%d", &carrera);

	if ( carrera == 1 ){
		cred = cred + 1;
	}

	printf("¿Quedaste en uno de los 3 primeros lugares?:\n1.-Si\n0.-No\nDigite su opción: ");
	scanf("%d", &lugar);

	if ( lugar == 1 ){
		cred = cred + 1;
	}

	printf("El total de créditos es: %d%s", cred , "\n");

}
