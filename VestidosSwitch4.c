/*
* Autor: Fatima Azucena MC
* Fecha: 20_01_2023
* Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>
int main (){

	//Declaración de variables
	int opcion;
	int cantidad;
	int total = 0;

	printf("Selecciona la talla de vestido");
	printf("\n1.-Talla chica\n2.-Talla mediana\n3.-Talla grande\nOpción: ");
	scanf("%d", &opcion);

	switch ( opcion ){
		case 1:
			printf("Haz selecionado la talla chica (Talla 7)¿Cuántos vestidos compraste?: ");
			scanf("%d", &cantidad);
			total = cantidad * 300;
		break;
		case 2:
			printf("Haz selecionado la talla mediana (Talla 10)¿Cuántos vestidos compraste?: ");
                        scanf("%d", &cantidad);
                        total = cantidad * 400;
		break;
		case 3:
			printf("Haz selecionado la talla grande (Talla 16)¿Cuántos vestidos compraste?: ");
                        scanf("%d", &cantidad);
                        total = cantidad * 500;

		break;
	}
	printf("Compraste %d%s%d%s" , cantidad , " vestidos y el total a pagar es: " , total , " pesos\n");

}
