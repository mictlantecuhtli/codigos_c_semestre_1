/*
* Autor: Fatima Azucena MC
* Fecha: 20_01_2023
* Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>
int main (){

	//Declaracion de variables
	int i;
	int edad;
	int sumaEdad = 0;
	int promedioEdades;

	for ( i = 1; i <= 3; i++ ){
		printf("Digite la edad del alumno %d%s" , i , ": ");
		scanf("%d", &edad);
		sumaEdad = sumaEdad + edad;
	}	
	promedioEdades = sumaEdad / 3;
	printf("El promedio de edades es: %d%s" , promedioEdades , "\n");

}
