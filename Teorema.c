#include <stdio.h>

// Definimos la función f(x) que queremos integrar
double funcion(double x) {
    return x * x; // Ejemplo: f(x) = x^2
}

// Función para calcular la antiderivada F(x) mediante el método de rectángulos (aproximación)
double antiderivada(double a, double b, int n) {
    double delta_x = (b - a) / n;
    double sum = 0;
    for (int i = 0; i < n; i++) {
        double xi = a + i * delta_x;
        sum += funcion(xi);
    }
    return sum * delta_x;
}

int main() {
    double a = 0; // Límite inferior del intervalo
    double b = 1; // Límite superior del intervalo
    int n = 100;   // Número de rectángulos (mayor n implica mayor precisión)

    // Calculamos la antiderivada F(b) - F(a)
    double integral_definida = antiderivada(a, b, n) - antiderivada(a, a, n);

    printf("El valor de la integral definida de f(x) desde %lf hasta %lf es: %lf\n", a, b, integral_definida);
    return 0;
}

