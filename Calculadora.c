/*
* Autor: Fátima Azucena MC
* Fecha: 14_02_2023
* Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>

int main (){

	//Declaración de variables
	int opcion;
	int salida;
	int numeroRe1 = 0;
	int numeroRe2 = 0;
	int numeroIm1 = 0;
	int numeroIm2 = 0;
	int suma1;
	int suma2;
	int resta1;
	int resta2;
	
	do {
		printf("\n\n\t\tBienvenido a la calculadora de números complejos");
        	printf("\n\t1.-Suma\n\t2.-Resta\n\t3.-Multiplicación\n\t4.-División");
        	printf("\n\tDigite su opción: ");
        	scanf("%d", &opcion);

		switch ( opcion ){
			case 1:
				printf("\n\tHaz elegido suma\n\tZ1 + Z2 = ( ZRe ) + ( ZIm )");
				printf("\n\tIngresa el primer número del conjunto de los números reales (Re1): ");
				scanf("%d", &numeroRe1);
				printf("\tIngresa el segundo número del conjunto de los números reales (Re2): ");
				scanf("%d", &numeroRe2);
                       		printf("\tIngresa el primer número del conjunto de los números imaginarios (Im1): ");
                        	scanf("%d", &numeroIm1);
                        	printf("\tIngresa el segundo número del conjunto de los números imaginarios (Im2): ");
                        	scanf("%d", &numeroIm2);
				suma1 = ( numeroRe1 + numeroRe2 );
				suma2 = ( numeroIm1 + numeroIm2 );
				printf("\tEl resultado de la suma es: %s%d%s%d%s%d%d%s%d%d%s", "( " , numeroRe1 , " " ,  numeroRe2 , " ) + ( " , numeroIm1 , numeroIm2 , " ) = " , suma1 , suma2 , "i");
			break;
			case 2:
				printf("\n\tHaz elegido resta\n\tZ1 - Z2 = ( Re1 - Re2 ) - ( Im1 - Im2 )");
				printf("\n\tIngresa el primer número del conjunto de los números reales (Re1): ");
                                scanf("%d", &numeroRe1);
                                printf("\tIngresa el segundo número del conjunto de los números reales (Im1): ");
                                scanf("%d", &numeroIm1);
                                printf("\tIngresa el primer número del conjunto de los números imaginarios (Re2): ");
                                scanf("%d", &numeroRe2);
                                printf("\tIngresa el segundo número del conjunto de los números imaginarios (Im2): ");
                                scanf("%d", &numeroIm2);
				resta1 = ( numeroRe1 + numeroRe2 );
				resta2 = - ( numeroIm1 + numeroIm2 );
				printf("\n\tEl resultado de la resta es: %s%d%s%d", "( " , resta1 , " "  , resta2 );
				
			break;
			case 3:
				printf("\n\tHaz elegido multiplicación");
			break;
			case 4:
				printf("\n\tHaz elegido división");
			break;
			default:
				printf("\n\tOpción inválida");
			break;
		}
	printf("\n\n\t¿Desea volver al menú?\n\t1.-Si\n\t0.-No\n\tDigite su opción: ");
	scanf("%d", &salida);
	} while ( salida == 1 );
	printf("\n\tHasta pronto");
}
