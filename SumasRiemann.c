// Fecha: 18_07_23
// Autor: Fátima Azucena MC
// Correo: fatimaazucenamartinez274@gmail.com

// Calcular en área bajo la curva mediante las sumas de Riemann

#include <stdio.h>

double funcion(double x) { // Inicio función que evalua a f(x)
    return x * x; // Fin función que evalúa a f(x)

double sumaRiemann(double a, double b, int n) { // Inicio función de suma
    double h = (b - a) / n;  // Ancho de cada subintervalo
    double suma = 0;
    double x;
    int i;

    // Calcular la suma de cada rectangula
    for (i = 0; i < n; i++) {
        x = a + i * h;  // Punto dentro del subintervalo
        suma += funcion(x) * h;
    }

    return suma;
} // Fin función suma de Riemann

int main() {
    double a = 0;  // Límite inferior del intervalo de integración
    double b = 1;  // Límite superior del intervalo de integración
    int n = 100;     // Número de subintervalos

    // Calcular la suma de Riemann
    double resultado = sumaRiemann(a, b, n);

    // Imprimir el resultado
    printf("El resultado de la suma de Riemann es: %f\n", resultado);

    return 0;
}

