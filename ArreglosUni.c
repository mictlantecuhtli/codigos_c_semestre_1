/*
 * Autor: Fátima Azucena Martínez Cadena
 * Fecha: 12_12_22
 * Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>

int main (){

	//Declaración de variables
	int i;
	int y;
	int z;
	int promedio;
	int promedioFinal;
	int arre1[5];
	char  titulos [][50] = {"Unidad 1","Unidad 2","Unidad 3","Unidad 4","Unidad 5"};

	for ( i = 0; i < 5; i++ ) {
		printf("Digite la calificación de la %s%s" , titulos[i] ,": ");
		scanf("%d", &arre1[i]);
		promedio = promedio + arre1[i];
	}
	promedioFinal = promedio / 5;
	for ( y = 0; y < 5; y++ ) { 
		printf("%s\t", titulos[y]);
	}
	printf("\n");
	for ( z = 0; z < 5; z++) {
		printf("%d\t\t  ", arre1[z]);
	}
	printf("\nEl promedio de la materia es de: %d" , promedioFinal);
}
