// Autor: Fátima Azucena MC
// Fecha: 31_01_2023
// Correo: fatimaazucenamartinez274@gmail.com

#include <stdio.h>

int main(){

	int numero;
	int i;
	int a;
	int b;
	int c;
	int d;
	int x = 0;
	int y = 1;
	int z = 1;

	printf("\nIngrese el número para serie de Fibonacci: ");
	scanf("%d", &numero);
	printf("\n");

	for ( i = 1; i <= numero; i++ ){
		c = 1;
		for ( a = 1; a < (( numero - i ) + 1 ); a++ ){
			printf(" ");
		}
		for ( b = 1; b <= i; b++ ){
			printf("%d%s", c , " ");
			c = c * ( i -b) / b;
		}
		printf("\n");
	}

	printf("\n\nSucesión\n\n");

	for ( d = 1; d <= numero; d++ ){
		z = ( x + y );
		printf("%d%s", z , " ");
		x = y;
		y = z;
	}
	
	printf("\n");

}


