/*
* Autor: Fatima Azucena MC
* Fecha: 20_01_2023
* Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>
int main (){

	//Declaración de variables
	int i;
	int y;
	int x;
	int j;

	for ( i = 1; i <= 8; i++ ){
		for ( y = 1; y <= 8 - i; y++ ){
			printf(" ");
		}
		for ( x = 1; x <= ( i * 2 ) - 1; x++){
			printf("*");
		}
		printf("\n");
	}
	for ( j = 1; j <= 4; j++){
		printf("      * * \n");
	}

}
