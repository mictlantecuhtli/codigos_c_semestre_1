/*
* Autor: Fatima Azucena MC
* Fecha: 20_01_2023
* Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>
int main() {

	//Declaración de variables
	int factorial;
	int i;
	int z = 1;
	int resFac;

	printf("¿Qué número desea ver su factorial?: ");
	scanf("%d", &factorial);

	for ( i = 1; i <= factorial; i++ ){
		resFac = z * i;
		printf("%d%s%d%s%d%s", z , " * " , i , " = " , resFac , "\n");
		z = resFac;
	}
	printf("El factorial de %d%s%d%s" , factorial , " es: " , z , "\n");

}
